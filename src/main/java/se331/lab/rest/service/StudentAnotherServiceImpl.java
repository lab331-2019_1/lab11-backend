package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.StudentAnotherDao;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentAnotherServiceImpl implements StudentAnotherService {
    @Autowired
    StudentAnotherDao studentAnotherDao;
    @Override
    public List<Student> getStudentByNameContains(String partOfName) {
        List<Student> students = studentAnotherDao.getAllStudent();
        List<Student> output = new ArrayList<>();
        for (Student student : students) {
            if (student.getName().toLowerCase().contains(partOfName.toLowerCase()) || student.getName().toUpperCase().contains(partOfName.toUpperCase())) {
                output.add(student);

            }

        }
        return output;
        //return studentAnotherDao.getStudentByNameContains(output);
    }


    @Override
    public List<Student> getStudentWhoseAdvisorNameIs(String advisorname) {
        List<Student> students = studentAnotherDao.getAllStudent();
        List<Student> output = new ArrayList<>();
        for (Student student: students) {
            String name = student.getAdvisor().getName();
            if (name.equalsIgnoreCase(advisorname)){
                output.add(student);
            }
        }
        return output;
        //return studentAnotherDao.getStudentWhoseAdvisorNameIs(advisorName);
    }

//    @Override
//    public List<Lecturer> getLecturerWhoseAdviseeGpaGreaterThan(double gpa) {
//        List<Lecturer> lecturers = lecturerAnotherDao.getAllLecturer();
//        List<Lecturer> output = new ArrayList<>();
//        for (Lecturer lecturer:
//             lecturers) {
//            double averageGpa = lecturer.getAdvisees().stream()
//                    .mapToDouble(Student::getGpa)
//                    .average()
//                    .orElse(Double.NaN);
//            if (averageGpa > gpa){
//                output.add(lecturer);
//            }
//
//        }
//        return output;
//    }
}
