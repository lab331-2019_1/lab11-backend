package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.StudentAnotherService;

@Controller
@Slf4j
public class StudentAnotherController {
    @Autowired
    StudentAnotherService studentAnotherService;
    @GetMapping("/studentPartOfName/{name}")
    public ResponseEntity getLectureBySurname(@PathVariable String name) {
        log.info("the controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(this.studentAnotherService.getStudentByNameContains(name)));
    }
//    @GetMapping("/lectureWithStudentGpa/{gpa}")
//    public ResponseEntity getLectureBySurname(@PathVariable Double gpa) {
//        log.info("the controller is call");
//        return ResponseEntity.ok(MapperUtil.INSTANCE.getLecturerDto(this.studentService.getStudentWhoseAdvisorNameIs(gpa)));


    @GetMapping("/studentAdvisorNameIs/{advisorname}")
    public ResponseEntity getStudentWhoseAdvisorNameIs(@PathVariable String advisorname) {
        log.info("the controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(this.studentAnotherService.getStudentWhoseAdvisorNameIs(advisorname)));
    }
}
